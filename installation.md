# Installation

There are two ways to install Xen Orchestra. If you are just a user and not a developer, please consider using the pre-built appliance, as this is the easier way (XOA).

Be advised that our default user and password for a fresh install are **admin@admin.net** and **admin**. Remember to change the default password immediately after installation.