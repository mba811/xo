# XOA Trial Activation

Trial will allow you to test all available features in XOA, for 15 days.

The procedure is really simple:

* register and download a Free Edition [on our website](https://xen-orchestra.com/#/member)
* [deploy the virtual appliance](/doc/installation/xoa_installation.md)
* request a Trial in the "Settings" and "Update" view

The trial panel in "Settings"/"Update":

![](https://xen-orchestra.com/blog/content/images/2015/05/updater.png)

![](https://xen-orchestra.com/blog/content/images/2015/05/trial.png)
