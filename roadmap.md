# Roadmap

Please consider it as an indicative roadmap, things can change.

Item are expressed in a (roughly) decreasing priority order.

## Features

- [Interface redesign](https://xen-orchestra.com/blog/announcing-xen-orchestra-5-x/) (in progress)
- [Alerts](alerts.md) (in progress)
- [Continuous Replication](continuous_replication.md)(in progress)
- [Self Service, quotas](self_service.md)
- [Auto scalability](auto_scalability.md)
- [Load balancing](load_balancing.md)
- [Forecaster](forecaster.md)


## Fixes

- Better browser scalability for huge infrastructures
- [Resolve known bugs](https://github.com/vatesfr/xo/blob/master/doc/known_bugs/README.md)
