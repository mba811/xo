# Trial activation

Trial will allow you to test all available features in XOA, for 15 days.

The procedure is really simple:

* register and download a Free Edition [on our website](https://xen-orchestra.com/#/member)
* [deploy the virtual appliance](xoa.md)
* request a Trial in the "Settings" and "Update" view

The trial panel in "Settings"/"Update":

![](https://xen-orchestra.com/blog/content/images/2015/05/updater.png)

![](https://xen-orchestra.com/blog/content/images/2015/05/trial.png)

As soon you clicked on "Start trial", you'll have a new update available, which will unlock all the features.

At the end of the trial, you'll be rollback to the Free version.